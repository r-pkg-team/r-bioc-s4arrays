Source: r-bioc-s4arrays
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-s4arrays
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-s4arrays.git
Homepage: https://bioconductor.org/packages/S4Arrays/
Standards-Version: 4.6.2
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-matrix,
               r-cran-abind,
               r-bioc-biocgenerics (>= 0.45.2),
               r-bioc-s4vectors,
               r-bioc-iranges,
               r-cran-crayon,
               architecture-is-64-bit
Testsuite: autopkgtest-pkg-r

Package: r-bioc-s4arrays
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: foundation of array-like containers in Bioconductor
 The S4Arrays package defines the Array virtual class to be
 extended by other S4 classes that wish to implement a container
 with an array-like semantic. It also provides: (1) low-level
 functionality meant to help the developer of such container to
 implement basic operations like display, subsetting, or coercion
 of their array-like objects to an ordinary matrix or array, and
 (2) a framework that facilitates block processing of array-like
 objects (typically on-disk objects).
